//
//  xz_square.cpp
//  prog0-2_valentines
//
//  Created by afable on 12-04-30.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "xz_square.h"

XZSquare::XZSquare(void)
{
	sq[0].Set(0.0f, 0.0f, 0.0f);
	sq[1].Set(0.0f, 0.0f, 0.0f);
	sq[2].Set(0.0f, 0.0f, 0.0f);
	sq[3].Set(0.0f, 0.0f, 0.0f);
	sideLen = 0.0f;
}

XZSquare::XZSquare(const XZSquare& copy)
{
	sq[0] = copy.sq[0];
	sq[1] = copy.sq[1];
	sq[2] = copy.sq[2];
	sq[3] = copy.sq[3];
	center = copy.center;
	sideLen = copy.sideLen;
}

XZSquare::~XZSquare(void)
{
	// do-nothing silent destructor
}

void XZSquare::Init(const APoint& vLocation, const AVector& vForward, const AVector& vRight, const float fSideLen)
{
	sideLen = fSideLen;
	center = vLocation;
	Update(vLocation, vForward, vRight);
}

// set the square based on a center point in xyz, forward and right vector in xz, and square sideLen
void XZSquare::Update(const APoint& vLocation, const AVector& vForward, const AVector& vRight)
{
	center = vLocation;		// update center
	float hs = sideLen / 2.0f; // hs is half the side length
	
	// bounding boxes are flattened to the y=0 xz plane
	sq[0].Set(vLocation[0]+(vForward[0]*hs)-(vRight[0]*hs), 0.0f, vLocation[2]+(vForward[2]*hs)-(vRight[2]*hs));
	sq[1].Set(vLocation[0]+(vForward[0]*hs)+(vRight[0]*hs), 0.0f, vLocation[2]+(vForward[2]*hs)+(vRight[2]*hs));
	sq[2].Set(vLocation[0]-(vForward[0]*hs)-(vRight[0]*hs), 0.0f, vLocation[2]-(vForward[2]*hs)-(vRight[2]*hs));
	sq[3].Set(vLocation[0]-(vForward[0]*hs)+(vRight[0]*hs), 0.0f, vLocation[2]-(vForward[2]*hs)+(vRight[2]*hs));
	
}

APoint& XZSquare::operator [] (const int x)
{
	return sq[x];
}

const APoint& XZSquare::operator [] (const int x) const
{
	return sq[x];
}

std::ostream& operator << (std::ostream& os, const XZSquare& sq)
{
	os << "\tbox with sideLen: " << sq.sideLen << std::endl;
	os << "\t" << sq[0] << "\t" << sq[1] << std::endl;
	os << "\t" << sq[2] << "\t" << sq[3];
	
	return os;
}



