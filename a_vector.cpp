//
//  a_vector.cpp
//
//  Created by afable on 12-02-08.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "a_vector.h"


/**********************************************
 * CONSTRUCTORS & DESTRUCTOR
 **********************************************/
AVector::AVector(void)
{
	f[0] = f[1] = f[2] = f[3] = 0.0f;
}

AVector::AVector(const float x, const float y, const float z, const float w)
{
	f[0] = x;
	f[1] = y;
	f[2] = z;
	f[3] = w;
}

AVector::AVector(const AVector& copy)
{
	f[0] = copy.f[0];
	f[1] = copy.f[1];
	f[2] = copy.f[2];
	f[3] = copy.f[3];
}

AVector::~AVector(void)
{
	//	do-nothing silent destructor
}



/**********************************************
 * SETTERS
 **********************************************/
void AVector::Set(const float x, const float y, const float z, const float w)
{
	f[0] = x;
	f[1] = y;
	f[2] = z;
	f[3] = w;
}

void AVector::SetY(const float y)
{
	f[1] = y;
}

void AVector::Normalize(void) // makes vector unit length
{
	float magnitude = GetMagnitude();
	
	for (int i = 0; i < 4; ++i)
		f[i] /= magnitude;
}

void AVector::Zero(void) // assigns zeros to all vector values
{
	f[0] = f[1] = f[2] = f[3] = 0.0f;
}



/**********************************************
 * GETTERS
 **********************************************/
float AVector::GetMagnitude(void) // returns length of vector
{
	if ( f[3] == 0 )
		return (1.0f / InvSqrt(f[0]*f[0] + f[1]*f[1] + f[2]*f[2] + f[3]*f[3]));
	else
	{
		std::cerr << "Cannot get magnitude of a point! AVector::GetMagnitude() failed." << std::endl;
		return 1.0f;
	}
}

AVector AVector::GetUnitVector(void)
{
	AVector u = *this;
	u.Normalize();
	return u;
}



/**********************************************
 * MEMBER FUNCTIONS
 **********************************************/
bool AVector::IsPoint(void) const
{
	return f[3] == 1.0f;
}



/**********************************************
 * OPERATOR OVERLOADS
 **********************************************/
AVector& AVector::operator = (const AVector& v)	// default assignment operator
{
	f[0] = v.f[0];
	f[1] = v.f[1];
	f[2] = v.f[2];
	f[3] = v.f[3];
	
	return *this;
}

// cannot use for cout << (happy + sad); returned copy gets destroyed before going to cout? only works for assignment
AVector AVector::operator + (const AVector& v) const
{
	return AVector(f[0]+v.f[0], f[1]+v.f[1], f[2]+v.f[2], f[3]+v.f[3]);
}

AVector AVector::operator - (const AVector& v) const
{
	return AVector(f[0] - v.f[0],
				   f[1] - v.f[1],
				   f[2] - v.f[2],
				   f[3] - v.f[3]);
}

AVector AVector::operator * (const float s) const
{
	return AVector(s*f[0], s*f[1], s*f[2], s*f[3]);
}

const AVector& AVector::operator += (const AVector& v)
{
	f[0] += v.f[0];
	f[1] += v.f[1];
	f[2] += v.f[2];
	f[3] += v.f[3];
	
	return *this;
}

const AVector& AVector::operator -= (const AVector& v)
{
	f[0] -= v.f[0];
	f[1] -= v.f[1];
	f[2] -= v.f[2];
	f[3] -= v.f[3];
	
	return *this;
}

const AVector& AVector::operator *= (const float scalar)
{
	f[0] *= scalar;
	f[1] *= scalar;
	f[2] *= scalar;
	
	return *this;
}

bool AVector::operator == (const AVector& v)
{
	return (f[0] == v.f[0] && f[1] == v.f[1] && f[2] == v.f[2] && f[3] == v.f[3]);
}

bool AVector::operator != (const AVector& v)
{
	return (f[0] != v.f[0] || f[1] != v.f[1] || f[2] != v.f[2] || f[3] != v.f[3]);
}

float& AVector::operator [] (const int x)
{
	return f[x];
}

const float& AVector::operator [] (const int x) const
{
	return f[x];
}



/**********************************************
 * FRIEND OPERATOR OVERLOADS
 **********************************************/
AVector operator * (const float s, const AVector& v)
{
	return v*s;
}

std::ostream& operator << (std::ostream& os, const AVector& v)
{
	os << "{" << v.f[0] << "," << v.f[1] << "," << v.f[2] << "}";
	
	return os;
}



/**********************************************
 * PUBLIC FUNCTIONS
 **********************************************/
AVector CrossProduct(const AVector& lhv, const AVector& rhv)
{
	if (lhv[3] == 0 && rhv[3] == 0)
		return AVector(lhv[1]*rhv[2] - lhv[2]*rhv[1],
					   lhv[2]*rhv[0] - lhv[0]*rhv[2],
					   lhv[0]*rhv[1] - lhv[1]*rhv[0],
					   0);
	else
	{
		std::cerr << "Can only cross_product vectors! Aborting.\n";
		return AVector(0.0f, 0.0f, 0.0f, 0.0f);
	}
}

// return a normal unit vector from the cross product of three points (also the equation of a plane)
AVector FindNormal(const APoint& p1, const APoint& p2, const APoint& p3)
{
	if ( !p1.IsPoint() || !p2.IsPoint() || !p3.IsPoint() )
	{
		std::cerr << "All vectors must be points! FindNormal() failed." << std::endl;
		return AVector();
	}
	AVector u = CrossProduct(p2 - p1, p3 - p1);
	u.Normalize();
	return u;
}

// return the equation of a plane (Ax + By + Cz + D = 0) (has to do with determinants)
// points should be given in cw order with normal pointing out of cw face
AVector GetPlaneEquation(const APoint& p1, const APoint& p2, const APoint& p3)
{	
	if ( !p1.IsPoint() || !p2.IsPoint() || !p3.IsPoint() )
	{
		std::cerr << "All vectors must be points! GetPlaneEquation() failed." << std::endl;
		return AVector();
	}
	
	// get unit normal to plane
	AVector u = CrossProduct(p2 - p1, p3 - p1);
	
	// calculate the d value of the plane and add to u and you have the equation of a plane!
	u[3] = -(u[0]*p2[0] + u[1]*p2[1] + u[2]*p2[2]);
	
	return u;
}



/**********************************************
 * EFFICIENT FUNCTIONS
 **********************************************/
// inverse sqrt used in Quake 3 engine
float InvSqrt(float x)
{
	float xhalf = 0.5f*x;
	union
	{
		float x;
		int i;
	} u;
	u.x = x;
	u.i = 0x5f3759df - (u.i >> 1);		// gives initial guess y0
	x = u.x * (1.5f - xhalf * u.x * u.x);		// repeat to increase accuracy (newton)
	
	return x;
}

// turns vector v into a unit vector
void Norm(AVector& v)	// a "shorter" version of Normalize()
{
	float magnitude = (1.0f / InvSqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2])); // v[3] should be 0 (vector)
	
	v[0] /= magnitude;
	v[1] /= magnitude;
	v[2] /= magnitude;
	// v[3] should be 0 (vector)
	
	return;
}

void GetUnitVector(AVector& dst, AVector& v)
{
	dst = v;
	Norm(dst);
	
	return;
}

void Add(AVector& dst, const AVector& v1, const AVector& v2)
{
	dst[0] = v1[0] + v2[0];
	dst[1] = v1[1] + v2[1];
	dst[2] = v1[2] + v2[2];
	dst[3] = v1[3] + v2[3];
	
	return;
}

void Subtract(AVector& dst, const AVector& v1, const AVector& v2)
{
	dst[0] = v1[0] - v2[0];
	dst[1] = v1[1] - v2[1];
	dst[2] = v1[2] - v2[2];
	dst[3] = v1[3] - v2[3];
	
	return;
}

void Multiply(AVector& dst, const AVector& v1, const float s)
{
	dst[0] = v1[0] * s;
	dst[1] = v1[1] * s;
	dst[2] = v1[2] * s;
	dst[3] = v1[3] * s;
	
	return;
}

void CrossProduct(AVector& dst, const AVector& lhv, const AVector& rhv)
{
	dst[0] = lhv[1]*rhv[2] - lhv[2]*rhv[1];
	dst[1] = lhv[2]*rhv[0] - lhv[0]*rhv[2];
	dst[2] = lhv[0]*rhv[1] - lhv[1]*rhv[0];
	dst[3] = 0;
	
	return;
}

float DotProduct(const AVector& lhv, const AVector& rhv)
{
	return lhv[0]*rhv[0] + lhv[1]*rhv[1] + lhv[2]*rhv[2] + lhv[3]*rhv[3];
}

void FindNormal(AVector& n, const APoint& p1, const APoint& p2, const APoint& p3)
{
	CrossProduct(n, p2 - p1, p3 - p1);	// right hand rule (create a triangle)
	Norm(n);
	
	return;
}

void GetPlaneEquation(AVector& u, const APoint& p1, const APoint& p2, const APoint& p3)
{
	CrossProduct(u, p2 - p1, p3 - p1);	// right hand rule (create a triangle)
	Norm(u);
	
	// calculate the d value of the plane and add to u and you have the equation of a plane!
	u[3] = -(u[0]*p2[0] + u[1]*p2[1] + u[2]*p2[2]);
	
	return;
}

void Project(AVector& dst, const AVector& u, const AVector& v)
{
	
	
	
}

float absv(const float f)
{
	if ( f < 0.0f )
		return -f;
	else
		return f;
}























