# created by @afable
#
# this Makefile only uses the OpenGL framework installed in /System/Library/Frameworks/OpenGL.framework
# '$<' is the name of the first prerequisite. for more info search for 'automatic variables' in http://www.gnu.org/software/make/manual/make.html#Automatic-Variables
#
#	-lSDLmain links the libSDLmain.a library in /usr/local/lib so you don't need SDLMain.m and SDLMain.h
#	-public frameworks can be found in /Library/Frameworks (mac osx)
#	-private frameworks can be found in /System/Library/Frameworks but it is not recommended to put public frameworks here (mac osx)

c++ = g++
cflags = -g -Wall
linuxlibs = -lSDLmain `sdl-config --cflags` `sdl-config --libs` -lGL -lSDL_mixer -lGLU  -lGL	# for linux
frameworks = -lSDLmain -framework Cocoa -framework SDL -framework SDL_mixer -framework OpenGL	# for mac
headers = -I/Library/Frameworks/SDL.framework/Headers
objs = main.o a_actor.o a_block.o a_boat.o a_elevator.o a_eye.o a_matrix.o a_monster.o a_timer.o a_vector.o a_water_monster.o xz_square.o xz_circle.o glm.o
prog = game_engine
optimizations = -O2		# post-debugging optimization

$(prog): $(objs)
	$(c++) $(cflags) $(objs) $(frameworks) -o $(prog) $(optimizations)

%.o: %.cpp
	$(c++) $(cflags) -c $<	$(headers)		# automatic variables

clean:
	rm -f $(objs)
	rm -f $(prog)


# for linux
linux: $(objs)
	$(c++) $(cflags) $(objs) $(linuxlibs) -o $(prog) $(optimizations)



