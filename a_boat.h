//
//  a_boat.h
//  prog0-2_valentines
//
//  Created by afable on 12-05-08.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef A_BOAT_H_
#define A_BOAT_H_

#include "a_elevator.h"



// init values for boats
const float BOAT_SIDE_LENGTH = 1.0f;
const float BOAT_RADIUS = BOAT_SIDE_LENGTH * SIN45 + ERROR_RANGE_SMALL;
const float BOAT_HEIGHT = 0.65f;
const float BOAT_TRANS_SPEED = 0.02f;
const float BOAT_ROT_SPEED = 1.0f;


/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	ABoat: a special kind of elevator: moving blocks that are player controlled transport vessels.
 *|
 *|	Class Hierarchy:
 *|
 *|		- AActor: base class for all actors within the scene including 'fake' eye.
 *|
 *|			- ABlock: a static block that makes up the ground terrain and island terrain.
 *|
 *|				- AElevator: moving blocks that make up moving platforms within game.
 *|
 *|					- ABoat: a special kind of elevator: moving blocks that are player controlled transport vessels.
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
class ABoat: public AElevator
{
private:
	
public:
	ABoat(void)
	{
		
	}
	
	~ABoat(void)
	{
		
	}
	
	/* VIRTUAL FUNCTION REDEFINITIONS */	
	// process incoming events: handle events (key input) and set object velocities
	void ProcessEvents(SDL_Event& event)
	{
		// after grabbing an event from the event queue
		switch (event.type)
		{		
			case SDL_KEYDOWN: switch (event.key.keysym.sym)
			{
				case SDLK_UP: fForwardVelocity = fTransSpeed;
					break;
				case SDLK_DOWN: fBackwardVelocity = NEGATIVE(fTransSpeed);
					break;
				case SDLK_LEFT: fRotYVelocity = fRotSpeed;
					break;
				case SDLK_RIGHT: fRotYVelocity = NEGATIVE(fRotSpeed);
					break;
					FLYING 
					(
				 case SDLK_KP1: fUpVelocity = fTransSpeed;
					 break;
				 case SDLK_KP0:	fDownVelocity = NEGATIVE(fTransSpeed);
					 break;					 
					 )
					
				default:
					break;
			}
				break;
				
			case SDL_KEYUP: switch (event.key.keysym.sym)
			{
				case SDLK_UP: fForwardVelocity = 0.0f;
					break;
				case SDLK_DOWN: fBackwardVelocity = 0.0f;
					break;
				case SDLK_LEFT: fRotYVelocity = 0.0f;
					break;
				case SDLK_RIGHT:fRotYVelocity = 0.0f;
					break;
					FLYING 
					(
				case SDLK_KP1: fUpVelocity = 0.0f;
					break;
				case SDLK_KP0: fDownVelocity = 0.0f;
					break;
					 )
					
				default:
					break;
			}
				break;
				
			case SDL_MOUSEBUTTONDOWN: // do something if mouse is clicked?
				break;
				
			default:
				break;
		}
	}
	
	// a helper function of AElevator::ProcessLogic() that handles the boat x,z boundaries
	void ProcessElevatorLogic(const AActor* eye)
	{
		if ( vLocation[0] < ::SEA_X_MINUS_BOUNDARY ||	// if boat exits rectangle sea, gravity pulls it to the earth
			vLocation[0] > ::SEA_X_PLUS_BOUNDARY ||
			vLocation[2] < ::SEA_Z_MINUS_BOUNDARY ||
			vLocation[2] > ::SEA_Z_PLUS_BOUNDARY )
		{
			SetGravity(::GRAVITY);
		}
		
		if ( vLocation[1] < ::SEA_Y_BOUNDARY )	// if boat has past the falling point of no return, reset it
		{
			DEBUGGING ( std::cerr << "boat fell off rectangle sea and was reset\n"; )
			Reset();
		}
	}
	
	/* FRIEND FUNCTIONS */
	friend std::ostream& operator << (std::ostream& os, const ABoat& bo)
	{
		os << "** boat **: " << bo.nId << std::endl;
		os << "\tvLocation: " << bo.vLocation << std::endl;
		os << "\tvForward: " << bo.vForward << std::endl;
		os << "\tvUp: " << bo.vUp << std::endl;
		os << "\tvRight: " << bo.vRight << std::endl;
		os << "Velocities (x, y, z): (" << bo.fRightVelocity + bo.fLeftVelocity << "," << bo.fUpVelocity + bo.fDownVelocity << "," << bo.fForwardVelocity + bo.fBackwardVelocity << "), fGravity = " << bo.fGravity << "\n";
		os << "Rot (x, y): (" << bo.fRotX << ", " << bo.fRotY << ")\n";
		os << "boundingBox:\n";
		os << bo.boundingBox << std::endl;
		os << "boundingCircle:\n";
		os << bo.boundingCircle << std::endl;
		os << "bCollision: " << bo.bCollision << std::endl;
		os << "bTopCollision: " << bo.bTopCollision << std::endl;
		os << "bBotCollision: " << bo.bBotCollision << std::endl;
		os << "bInteractCollision: " << bo.bInteractCollision << std::endl;		
		os << "nSquished: " << bo.nSquished << std::endl;		
		os << "fTopY: " << bo.fTopY << ", fBotY: " << bo.fBotY << std::endl;
		
		return os;
	}
	
	
	
	
	
};	// end of class ABoat


#endif	// A_BOAT_H_





