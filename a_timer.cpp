//
//  a_timer.cpp
//
//  Created by afable on 12-02-13.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "a_timer.h"

ATimer::ATimer(void)
{
	start_ticks_ = 0;
	started_ = false;
	paused_ = false;
}

ATimer::~ATimer(void)
{
	//	silent do-nothing destructor
}

void ATimer::Start(void)
{
	started_ = true;
	paused_ = false;
	start_ticks_ = SDL_GetTicks();
}

void ATimer::Stop(void)
{
	started_ = false;
	paused_ = false;
	start_ticks_ = 0;
	paused_ticks_ = 0;
}

void ATimer::Pause(void)
{
	//	if timer running and isn't paused
	if ( started_ && !(paused_) )
	{
		//	pause timer
		paused_ = true;
		
		//	IMPORTANT: get paused_ticks_ before changing start_ticks_
		paused_ticks_ = SDL_GetTicks() - start_ticks_;
	}
}

void ATimer::UnPause(void)
{
	// if timer running and is paused
	if ( started_ && paused_ )
	{
		paused_ = false;
		start_ticks_ = SDL_GetTicks() - paused_ticks_;
	}
	
}

int ATimer::GetTicks(void) const
{
	if ( started_ )
	{
		if ( paused_ )
			return paused_ticks_;
		else
			return SDL_GetTicks() - start_ticks_;
	}
	else
		return 0;
}

bool ATimer::IsStarted(void)
{
	return started_;
}

bool ATimer::IsPaused(void)
{
	return paused_;
}

std::ostream& operator << (std::ostream& os, const ATimer& t)
{
	if ( t.started_ )
	{
		if ( t.paused_ )
			os << "Paused time: " << t.GetTicks();
		else
			os << "Ticks elapsed: " << t.GetTicks();
	}
	else
		os << "Timer not started";
	return os;
}


