
Created 2012-04-22
Erik Afable
Project: Simple Game Engine

---------------------------------------
Description:
---------------------------------------

This game engine was created in OpenGL, SDL, and C++. Simple collision detection is done by flattening circle and square collisions in xz plane. generally, if the y value of objects are at a level where 2 objects can interact, a bounding circle collision test is first done (since it is faster) and if passes, a bounding square collision test is done. a base actor class was created and all objects are instantiations of derived classes from the base actor class.

---------------------------------------
Compile/Run instructions:
---------------------------------------

You must have the SDL framework and SDL library installed (libSDLmain.a) to make and run this program. Run make from /src. This project was built only for the MAC/OSX platform.

This program can be run after making the build in /src:
./game_engine	--> will run the program

Features while running the program:
'q' --> quit the program.

3rd person point of view character:
hold left mouse button and move mouse --> rotate camera left, right, up, and down.
'w' --> move 3ppov forward or stop if moving backward.
's' --> move 3ppov backward or stop if moving forward.
'a' --> strafe 3ppov left or stop if strafing right.
'd' --> strafe 3ppov right or stop if strafing left.

boat:
'up arrow' --> move boat forward or stop if moving backward.
'down arrow' --> move boat backward or stop if moving forward.
'left arrow' --> rotate boat direction left
'right arrow' --> rotate boat direction right

elevator:
'NUMPAD 1' --> move elevator up in y.
'NUMPAD 0' --> move elevator down in y.


---------------------------------------
Source Acknowledgement:
---------------------------------------

a_gooch.h
.ppm file loader for texture mapping
Dr. Amy Gooch, 2011
http://webhome.csc.uvic.ca/~agooch/


glm.h, glm.c
Wavefront OBJ model file format reader/writer/manipulator
Nate Robins, 1997
ndr@pobox.com, http://www.pobox.com/~ndr/


various simple .obj models
Jason Siefken
https://github.com/siefkenj













