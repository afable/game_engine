//
//  a_monster.h
//  prog0-2_valentines
//
//  Created by afable on 12-05-02.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//


#ifndef A_MONSTER_H_
#define A_MONSTER_H_

#include "a_actor.h"


// init values for monsters
const float MONSTER_HEIGHT = 1.0f;	// make monsters smaller than eye so hits work properly
const float MONSTER_RADIUS = 0.5f;
const float MONSTER_TRANS_SPEED = 0.02f;
const float MONSTER_SPEEDY_SPEED = 0.05f;
const float MONSTER_ROT_SPEED = 2.0f;

const int MONSTER_JUMP_DELAY_MAX = 166;



/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	AMonster: base class for a general monster that interacts with blocks the same way the
 *|		eye does. monsters do not interact with eachother. if a monster interacts at level with
 *|		eye the monster does damage to eye. eye handles all of the  monster2eye interaction, so
 *|		monster collisions are only tested against blocks.
 *| 
 *|	Class Hierarchy:
 *|
 *|		- AActor: base class for all actors within the scene including 'fake' eye.
 *|
 *|			- AMonster: base class for a general monster.
 *|
 *|				- AWaterMonster: a monster that stays in the sea and jumps on approaching boats.
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
class AMonster: public AActor
{
private:
	
public:
	/* CONSTRUCTORS & DESTRUCTOR */
	AMonster(void) : AActor()
	{
		
	}
	
	virtual ~AMonster(void)
	{
		// silent do-nothing destructors
	}
	
	
	/* VIRTUAL FUNCTION REDEFINITIONS */
	void Init(int monsterId, float xloc, float yloc, float zloc, float height, float radius, float transSpeed, float rotSpeed, float xrot, float yrot)
	{
		nId = monsterId;	// means not initialized
		
		// monster position, size, and orientation
		vLocation.Set(xloc, yloc, zloc, 1.0f);
		vForward.Set(0.0f, 0.0f, -1.0f); // LoadIdentity() overwirtes this. OpenGL eye default looks into screen (-z)
		vUp.Set(0.0f, 1.0f, 0.0f);	// up is up (+y)
		CrossProduct(vRight, vForward, vUp);	// right is crossproduce of forward & up
		fHalfHeight = height / 2.0f;
		fTopY = vLocation[1] + fHalfHeight;
		fBotY = vLocation[1] - fHalfHeight;
		fRotX = xrot;
		fRotY = yrot;
		
		// collision data
		boundingCircle.Init(vLocation, radius);		// eye only uses a bounding circle
		
		// velocities and speeds
		fGravity = GRAVITY;
		fTransSpeed = transSpeed;
		fForwardVelocity = transSpeed;
		fBackwardVelocity = 0.0f;
		fUpVelocity = 0.0f;
		fDownVelocity = 0.0f;
		fRightVelocity = 0.0f;
		fLeftVelocity = 0.0f;
		fRotSpeed = rotSpeed;
		fRotXVelocity = 0.0f;
//		fRotYVelocity = rotSpeed;
		
		// collision data
		bCollision = false;
		bTopCollision = false;
		bBotCollision = false;
		bInteractCollision = false;
		bEdge = false;
		bCorner = false;
		nSquished = SQUISH_AMOUNT_MAX;
		
		// jump data
		bCanJump = false;
		nJumpDelay = 0;	// start with no jump delay
		
		// hit data
		bHit = false;
		
		// heart state
		bHeart = false;
		bDelete = false;
		
		// set reset values
		fXLoc = xloc;
		fYLoc = yloc;
		fZLoc = zloc;
		fXRot = xrot;
		fYRot = yrot;
	}
	
	void Reset(void)
	{
		//nId = monsterId;	// means not initialized
		
		// monster position, size, and orientation
		vLocation.Set(fXLoc, fYLoc, fZLoc, 1.0f);
		vForward.Set(0.0f, 0.0f, -1.0f); // LoadIdentity() overwirtes this. OpenGL eye default looks into screen (-z)
		vUp.Set(0.0f, 1.0f, 0.0f);	// up is up (+y)
		CrossProduct(vRight, vForward, vUp);	// right is crossproduce of forward & up
		//fHalfHeight = height / 2.0f;
		fTopY = vLocation[1] + fHalfHeight;
		fBotY = vLocation[1] - fHalfHeight;
		fRotX = fXRot;
		fRotY = fYRot;
		
		// collision data
		boundingCircle.Init(vLocation, ::MONSTER_RADIUS);		// eye only uses a bounding circle
		
		// velocities and speeds
		fGravity = GRAVITY;
		//fTransSpeed = transSpeed;
		fForwardVelocity = 0.0f;
		fBackwardVelocity = 0.0f;
		fUpVelocity = 0.0f;
		fDownVelocity = 0.0f;
		fRightVelocity = 0.0f;
		fLeftVelocity = 0.0f;
		//fRotSpeed = rotSpeed;
		fRotXVelocity = 0.0f;
		fRotYVelocity = 0.0f;
		
		// collision data
		bCollision = false;
		bTopCollision = false;
		bBotCollision = false;
		bInteractCollision = false;
		bEdge = false;
		bCorner = false;
		nSquished = SQUISH_AMOUNT_MAX;
		
		// jump data
		bCanJump = false;
		nJumpDelay = 0;	// start with no jump delay
		
		// hit data
		bHit = false;
		
		// heart state
		bHeart = false;
		bDelete = false;
	}
	
	// process incoming events: handle events (key input) and set object velocities
	void ProcessEvents(SDL_Event& event)
	{
		// after grabbing an event from the event queue
		switch (event.type)
		{
			case SDL_KEYDOWN: switch (event.key.keysym.sym)
			{
				case SDLK_KP8: fForwardVelocity = fTransSpeed;
					break;
				case SDLK_KP5: fBackwardVelocity = NEGATIVE(fTransSpeed);
					break;
				case SDLK_KP4: fRotYVelocity = fRotSpeed;
					break;
				case SDLK_KP6: fRotYVelocity = NEGATIVE(fRotSpeed);
					break;
				case SDLK_KP_ENTER:
					if ( bCanJump ) // only jump if monster can
					{
						bCanJump = false;	// monster can't jump while in the air
						fUpVelocity = JUMP_SPEED;	// increase y velocity
						nJumpDelay = MONSTER_JUMP_DELAY_MAX;	// reset your next jump delay to max
					}
					break;
					
				default:
					break;
			}
				break;
				
			case SDL_KEYUP: switch (event.key.keysym.sym)
			{
				case SDLK_KP8: fForwardVelocity = 0.0f;
					break;
				case SDLK_KP5: fBackwardVelocity = 0.0f;
					break;
				case SDLK_KP4: fRotYVelocity = 0.0f;
					break;
				case SDLK_KP6: fRotYVelocity = 0.0f;
					break;
				case SDLK_KP_ENTER:	// jump is a press once deal
					break;
					
				default:
					break;
			}
				break;
				
			case SDL_MOUSEBUTTONDOWN: // do something if mouse is clicked?
				break;
				
			default:
				break;
		}
	}
	
	
	
	/*----------------------------------------------------------
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|
	 *| 
	 *| MONSTER TO BLOCK/ELEVATOR COLLISIONS IN Y, X, Z: same as eye2block interactions.
	 *|
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|_________________________________________________________
	 */
	// helper function to ProcessLogic() that processes any block type collisions in y
	void ProcessMonster2BlockCollisionY(const AActor* block)
	{
		/**************************************************
		 * MONSTER TO BLOCK CIRCLE-TO-CIRCLE COLLISIONS Y: collisions with blocks outter bounding circle
		 **************************************************/	
		// if there is a collision with a block's outter bounding circle, then let's test the box
		if ( Circle2CircleCollision(boundingCircle, block->boundingCircle) )
		{
			/**************************************************
			 * MONSTER TO BLOCK CIRCLE-TO-SQUARE COLLISIONS Y: collisions with blocks inner bounding box
			 **************************************************/
			if ( Circle2SquareCollision(boundingCircle, block->boundingBox) )	// if not while since y logic only happens once a frame and only if there is a collision
			{
				bCollision = true;
				
				/**************************************************
				 * MONSTER TO BLOCK COLLISION Y: since vUp is always pointing in y axis
				 *	direction, y movements will not have any x or z values. since a collision
				 *	exists, we just need to check if monster is above, ontop, level, or below block
				 **************************************************/
				if ( fBotY > block->fTopY + ERROR_RANGE_SMALL + absv(fGravity) ||
					fTopY < block->fBotY - JUMP_SPEED ) // monster high above or far below block
				{
					// allow the y movement since monster very high from this block (already updated)
				}
				else if ( fBotY >= block->fTopY - ERROR_RANGE_SMALLEST - absv(fGravity) )	// else if monster standing ontop of block
				{
					if ( bBotCollision )	// if there was already a collision with blocks (e.g. monster standing between two blocks in same y value) don't translate back since it has already been done
					{
						// do nothing since has been handled already
					}
					else	// if this is the first block to collide with monster's bottom, translate monster ontop of block (stand on block or fall from hitting block)
					{
						while ( fBotY <= block->fTopY + ERROR_RANGE_SMALL )	// keep eye ontop of block
						{
							MoveUpY(absv(fUpVelocity + fDownVelocity + fGravity));	// translate monster ontop of block (add hit velocity in case just hit)
							// update top and bottom boundaries based on monster new height
							fTopY = vLocation[1] + fHalfHeight;
							fBotY = vLocation[1] - fHalfHeight;
							
							--nSquished;
							if ( nSquished < 0 ) // if monster has been squished between two blocks;
							{
								DEBUGGING
								(
								 std::cerr << *this << std::endl;
								 std::cerr << *block << std::endl;
								 for (int i=0;i<999;++i) std::cerr << "resetting from monster standing on block y\n";
								 )
								Reset();
								break;
							}
						}
						bBotCollision = true;		// now monster on multiple blocks will not move monster up
					}
				}
				else if ( fTopY <= block->fBotY + JUMP_SPEED )	// monster's head butting block, use gravity and jump speed for relative error (jump speed gives us max range monster will headbutt block)
				{
					if ( bTopCollision )	// handle monster headbutting multiple blocks
					{
						// do nothing since handled already
					}
					else	// first time monster is headbutting a block in this frame
					{
						while ( fTopY >= block->fBotY )	// handle being squished up
						{
							fUpVelocity = -fUpVelocity;	// change up velocity direction to down!
							MoveUpY(fUpVelocity + fDownVelocity + GRAVITY);	// want monster to move down from headbutt
							fTopY = vLocation[1] + fHalfHeight;
							fBotY = vLocation[1] - fHalfHeight;
							bTopCollision = true;
							
							--nSquished;
							if ( nSquished < 0 ) // if monster has been squished between two blocks;
							{
								DEBUGGING 
								(
								 std::cerr << *this << std::endl;
								 std::cerr << *block << std::endl;
								 for (int i=0;i<999;++i) std::cerr << "resetting from monster headbutting block y\n";
								 )
								Reset();
								break;
							}
						}
					}
				}
				else	// monster is on same level as block
				{
					// should never get here from a y movement
				}
			}
		}
	}
	
	// helper function to ProcessLogic() that processes any block type collisions in X
	void ProcessMonster2BlockCollisionX(const AActor* block)
	{
		/**************************************************
		 * MONSTER TO BLOCK CIRCLE-TO-CIRCLE COLLISIONS X: collisions with blocks outter bounding circle
		 **************************************************/
		// if there is a collision with a block's outter bounding circle, then let's test the box
		if ( Circle2CircleCollision(boundingCircle, block->boundingCircle) )
		{	
			/**************************************************
			 * MONSTER TO BLOCK CIRCLE-TO-SQUARE COLLISIONS X: collisions with blocks inner bounding box
			 **************************************************/
			while ( Circle2SquareCollision(boundingCircle, block->boundingBox) )
			{
				bCollision = true;
				
				/**************************************************
				 * MONSTER TO BLOCK COLLISION X: divide into 2 cases, either monster is on
				 *	a level that can interact with block (bot of monster below top of block or top
				 *	of monster above bot of block) or monster is above/below that level
				 **************************************************/
				if ( fBotY <= block->fTopY && fTopY >= block->fBotY )	// level monster can interact with block
				{
					bInteractCollision = true;
					
					// get vector from block to monster
					Subtract(pusher2pushee, vLocation, block->vLocation);
					
					// find the larger component and translate back in that direction (directly away from block)
					if ( absv(pusher2pushee[0]) > absv(pusher2pushee[2]) )
						MoveDirectionX(pusher2pushee, COLLISION_SPEED);
					else
						MoveDirectionZ(pusher2pushee, COLLISION_SPEED);
					
					boundingCircle.Update(vLocation);
				}
				else	// level monster cannot interact with block
				{
					// allow x movements
					break;	// while ( Circle2SquareCollision(boundingCircle, block->boundingBox) )
				}
				
				--nSquished;
				if ( nSquished < 0 )
				{
					DEBUGGING 
					(
					 std::cerr << *this << std::endl;
					 std::cerr << *block << std::endl;
					 for (int i=0;i<999;++i) std::cerr << "resetting from monster to block x\n";
					 )
					Reset();
					break;
				}
			}
		}
	}
	
	// helper function to ProcessLogic() that processes any block type collisions in Z
	void ProcessMonster2BlockCollisionZ(const AActor* block)
	{
		/**************************************************
		 * MONSTER TO BLOCK CIRCLE-TO-CIRCLE COLLISIONS Z: collisions with blocks outter bounding circle
		 **************************************************/
		// if there is a collision with a block's outter bounding circle, then let's test the box
		if ( Circle2CircleCollision(boundingCircle, block->boundingCircle) )
		{
			/**************************************************
			 * MONSTER TO BLOCK CIRCLE-TO-SQUARE COLLISIONS Z: collisions with blocks inner bounding box
			 **************************************************/
			while ( Circle2SquareCollision(boundingCircle, block->boundingBox) )
			{
				bCollision = true;
				
				/**************************************************
				 * MONSTER TO BLOCK COLLISION Z: divide into 2 cases, either monster is on
				 *	a level that can interact with block (bot of monster below top of block or top
				 *	of monster above bot of block) or monster is above/below that level
				 **************************************************/
				if ( fBotY <= block->fTopY && fTopY >= block->fBotY )	// level monster can interact with block
				{
					bInteractCollision = true;
					
					// get vector from block to monster
					Subtract(pusher2pushee, vLocation, block->vLocation);
					
					// find the larger component and translate back in that direction (directly away from block)
					if ( absv(pusher2pushee[0]) > absv(pusher2pushee[2]) )
						MoveDirectionX(pusher2pushee, COLLISION_SPEED);
					else
						MoveDirectionZ(pusher2pushee, COLLISION_SPEED);
					
					boundingCircle.Update(vLocation);
				}
				else	// level monster cannot interact with block
				{
					// allow z movements
					break;	// while ( Circle2SquareCollision(boundingCircle, block->boundingBox) )
				}
				
				--nSquished;
				if ( nSquished < 0 )
				{
					DEBUGGING 
					(
					 std::cerr << *this << std::endl;
					 std::cerr << *block << std::endl;
					 for (int i=0;i<999;++i) std::cerr << "resetting from monster to block z\n";
					 )
					Reset();
					break;
				}
			}
		}
	}
	
	// helper function of AMonster::ProcessLogic() that handles monster logic
	virtual void ProcessMonsterLogic(void)
	{
		if ( bEdge || bCorner )	// if at edge, move back and turn around
		{
			MoveForwardX(NEGATIVE(fForwardVelocity));	// first move back
			MoveForwardZ(NEGATIVE(fForwardVelocity));
			boundingCircle.Update(vLocation);
			
			fRotY += 180.0f;	// then turn around
			
			fRotY += 65.0f - (rand() * 0.0000000005f * 120.0f);	// generate a random turn value between ~[-55, 55] degrees
			
			bEdge = false;
			bCorner = false;
		}

		if ( 0.5f - (rand() * 0.0000000005f * 120.0f) > 0.0f )	// perform a jump at random
		{
			if ( bCanJump ) // only jump if monster can
			{
				bCanJump = false;	// monster can't jump while in the air
				fUpVelocity = JUMP_SPEED;	// increase y velocity
				nJumpDelay = MONSTER_JUMP_DELAY_MAX;	// reset your next jump delay to max
			}
		}
	}
	
	// helper function of AMonster::ProcessLogic() that handles jumps
	void ProcessJumps(void)
	{
		// decrement the jump delay
		--nJumpDelay;
		
		// if the jump delay has reached zero, eye is allowed to jump again
		if ( nJumpDelay < 0 )
			bCanJump = true;
		
		// decrement jump velocity
		fUpVelocity += JUMP_DECREMENT;
		
		// keep up velocity non-negative
		if ( fUpVelocity < 0.0f )
			fUpVelocity = 0.0f;
	}
	
	
	
	/*----------------------------------------------------------
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|
	 *| 
	 *| PROCESS LOGIC: 	move monsters, check collisions against blocks (and move back if necessary).
	 *|		monsters do not interact with other monsters and all eye2monster interactions are
	 *|		handled by eye, so we only need to check monster2block colliisons.
	 *|
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|_________________________________________________________
	 */
	void ProcessLogic(const std::vector<AActor*>& blocks, const std::vector<AActor*>& elevators)
	{	
		if ( bHeart )	// has monster been turned into a heart?
		{
			fForwardVelocity = 0.0f;
			fBackwardVelocity = 0.0f;
			fRotY += 1.0f;
			
			// reset unit vectors (leave location alone) and bound angles
			LoadIdentity();	
			
			// add rotational velocities
			fRotY += fRotYVelocity;
			
			// rotate forward and right vectors
			RotateY(fRotY);
			
			return;
		}
		
		// process monster logic
		ProcessMonsterLogic();
		
		// process jumps that the monster can make
		ProcessJumps();
		
		// add rotational velocities
		fRotY += fRotYVelocity;
		
		// reset unit vectors (leave location alone) and bound angles for sin/cos lookup tables
		LoadIdentity();
		
		// rotate forward and right vectors
		RotateY(fRotY);
		
		// reset squish amount to maximum
		nSquished = SQUISH_AMOUNT_MAX;
		
		// start all collisions as false
		bCollision = false;
		bTopCollision = false;	// handles monster ontop of many blocks
		bBotCollision = false;	// handles monster headbutting into many blocks
		bEdge = false;	// tests whether monster is at a block's edge
		bCorner = false;	// tests if monster is at a block's corner (rare to hit only a corner and no edge but possible)
		
		
		
		/*----------------------------------------------------------
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|
		 *| 
		 *| MOVE IN Y: move monster in y, update top and bot monster boundaries, and handle jumps. a jump
		 *|		delay is used to ensure that a jump happens at the press of the spacebar and jumps
		 *|		can only be pressed if you are ontop or climbing an actor. next, test collisions with
		 *|		blocks.
		 *|
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|_________________________________________________________
		 */
		// translate monster up and down by up, down velocities
		MoveUpY(fUpVelocity + fDownVelocity + fGravity); //  vForward={x,0,z}, vRight={x,0,z}, no y component
		
		// update top and bottom boundaries based on actor's height (only y translations affect this)
		fTopY = vLocation[1] + fHalfHeight;
		fBotY = vLocation[1] - fHalfHeight;
		
		
		/**************************************************
		 * MONSTER TO BLOCK COLLISIONS Y: process monster to block y collisions first. the y
		 *	collisions are split into 5 categories: monster high above block, monster ontop of block, monster
		 *	on block level, monster headbutting block, monster far below block.
		 **************************************************/
		for ( unsigned int i = 0; i < blocks.size(); ++i )	// process every block
			ProcessMonster2BlockCollisionY(blocks[i]);
		
		/**************************************************
		 * MONSTER TO ELEVATOR COLLISIONS Y: process monster to elevator y collisions. the y
		 *	collisions are split into 5 categories: monster high above block, monster ontop of block, monster
		 *	on block level, monster headbutting block, monster far below block.
		 **************************************************/
		for ( unsigned int i = 0; i < elevators.size(); ++i )
			ProcessMonster2BlockCollisionY(elevators[i]);
		
		
		
		/*----------------------------------------------------------
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|
		 *| 
		 *| MOVE IN X: move monster in x, update collision circle, test x collisions with actors
		 *|
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|_________________________________________________________
		 */
		// translate forward by forward velocity amount
		MoveForwardX(fForwardVelocity + fBackwardVelocity);
		
		// translate right by right velocity amount
		MoveRightX(fRightVelocity + fLeftVelocity);
		
		// update bounding circles (monster only uses a bounding circle and only xz translations affect this)
		boundingCircle.Update(vLocation);	// update circle's center
		
		/**************************************************
		 * MONSTER TO BLOCK COLLISIONS X: test collisions between monster and blocks outter bounding circle
		 *	first and if pass blocks inner bounding square. then check y value to determine if
		 *	monster is above, on-top of, below, or on same level as block
		 **************************************************/
		// process monster to block collisions
		for ( unsigned int i = 0; i < blocks.size(); ++i )
			ProcessMonster2BlockCollisionX(blocks[i]);
		
		/**************************************************
		 * MONSTER TO ELEVATOR COLLISIONS X: test collisions between monster and elevators outter bounding circle
		 *	first and if pass blocks inner bounding square. then check y value to determine if
		 *	monster is above, on-top of, below, or on same level as elevators
		 **************************************************/
		// process monster to elevator collisions
		for ( unsigned int i = 0; i < elevators.size(); ++i )
			ProcessMonster2BlockCollisionX(elevators[i]);
		
		
		
		/*----------------------------------------------------------
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|
		 *| 
		 *| MOVE IN Z: move monster in z, update collision circle, test z collisions with actors
		 *|
		 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		 *|_________________________________________________________
		 */
		// translate forward by forward velocity amount
		MoveForwardZ(fForwardVelocity + fBackwardVelocity);
		
		// translate right by right velocity amount
		MoveRightZ(fRightVelocity + fLeftVelocity);
		
		// update bounding circles (monster only uses a bounding circle and only xz translations affect this)
		boundingCircle.Update(vLocation);	// update circle's center
		
		/**************************************************
		 * MONSTER TO BLOCK COLLISIONS Z: test collisions between monster and blocks outter bounding circle
		 *	first and if pass blocks inner bounding square. then check y value to determine if
		 *	monster is above, on-top of, below, or on same level as block
		 **************************************************/
		// process monster to block collisions
		for ( unsigned int i = 0; i < blocks.size(); ++i )
			ProcessMonster2BlockCollisionZ(blocks[i]);
		
		/**************************************************
		 * MONSTER TO ELEVATOR COLLISIONS Z: test collisions between monster and elevators outter bounding circle
		 *	first and if pass blocks inner bounding square. then check y value to determine if
		 *	monster is above, on-top of, below, or on same level as elevators
		 **************************************************/
		// process monster to elevator collisions
		for ( unsigned int i = 0; i < elevators.size(); ++i )
			ProcessMonster2BlockCollisionZ(elevators[i]);
	}
	
	
	// render screen: draw objects final position in frame to screen
	void Display(GLuint lDrawing)
	{
		glPushMatrix(); // draw monster
		{
			// translate drawer to monster's location
			glTranslatef(vLocation[0], vLocation[1], vLocation[2]);
			
			// rotate drawer in y before drawing monster so monster rotates locally
			glRotatef(fRotY, 0.0f, 1.0f, 0.0f);
			
			// draw monster
			glCallList(lDrawing);	
		}
		glPopMatrix(); // end draw monster
	}
	
	/* FRIEND FUNCTIONS */
	friend std::ostream& operator << (std::ostream& os, const AMonster& mo)
	{
		os << "** monster **: " << mo.nId << std::endl;
		os << "\tvLocation: " << mo.vLocation << std::endl;
		os << "\tvForward: " << mo.vForward << std::endl;
		os << "\tvUp: " << mo.vUp << std::endl;
		os << "\tvRight: " << mo.vRight << std::endl;
		os << "Velocities (x, y, z): (" << mo.fRightVelocity + mo.fLeftVelocity << "," << mo.fUpVelocity + mo.fDownVelocity << "," << mo.fForwardVelocity + mo.fBackwardVelocity << "), fGravity: " << mo.fGravity << "\n";
		os << "Rot (x, y): (" << mo.fRotX << ", " << mo.fRotY << ")\n";
		os << "boundingCircle:\n";
		os << mo.boundingCircle << std::endl;
		os << "bCollision: " << mo.bCollision << std::endl;
		os << "bTopCollision: " << mo.bTopCollision << std::endl;
		os << "bBotCollision: " << mo.bBotCollision << std::endl;
		os << "bInteractCollision: " << mo.bInteractCollision << std::endl;
		os << "bEdge: " << mo.bEdge << std::endl;
		os << "bCorner: " << mo.bCorner << std::endl;
		os << "nSquished: " << mo.nSquished << std::endl;
		os << "bCanJump: " << mo.bCanJump << std::endl;
		os << "nJumpDelay: " << mo.nJumpDelay << std::endl;
		os << "fTopY: " << mo.fTopY << ", fBotY: " << mo.fBotY << std::endl;
		os << "bHeart: " << mo.bHeart << std::endl;
		os << "bDelete: " << mo.bDelete << std::endl;
		
		return os;
	}

	
};	// end of class AMonster


#endif	// A_MONSTER_H_






