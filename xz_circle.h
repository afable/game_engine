//
//  xz_circle.h
//  prog0-2_valentines
//
//  Created by afable on 12-05-03.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef XZ_CIRCLE_H_
#define XZ_CIRCLE_H_

#include <iostream>
#include "a_vector.h"

/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	XZCircle: circle object used for circle2circle and circle2square collisions ("a_actor.h")
 *|		in the xz plane with y=0
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
class XZCircle
{
private:
	
public:
	float rad;	// public since this class is really just a float and a point
	APoint center;
	
	// constructors and destructor
	XZCircle(void);
	XZCircle(const XZCircle& c);
	~XZCircle(void);
	
	// initialize circle with radius r
	void Init(APoint vLocation, const float r);
	
	// set circle's radius
	void Update(APoint vLocation);
	
	// send circle object to ostream object
	friend std::ostream& operator << (std::ostream& os, const XZCircle& c);
	
};	// end of class XZCircle


#endif	// CZ_CIRCLE_H_




