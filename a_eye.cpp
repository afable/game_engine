//
//  a_eye.cpp
//  prog0-2_valentines
//
//  Created by afable on 12-05-02.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "a_eye.h"

/* STATICS */
bool AEye::bResetEye;	// reset boat and elevator with eye
bool AEye::bNorthIslandCheckpoint;	// if eye reaches the north island, allow update to reset values
bool AEye::bCheckpointNotReached;	// to mark and print that the checkpoint has been reached

