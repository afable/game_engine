//
//  xz_square.h
//  prog0-2_valentines
//
//  Created by afable on 12-04-30.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef XZ_SQUARE_H_
#define XZ_SQUARE_H_

#include <iostream>
#include "a_vector.h"

/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	XZSquare: square object used for circle2square and square2square collisions ("a_actor.h")
 *|		in the xz plane with y=0
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
class XZSquare
{
private:
	// enum { farLeft=0, farRight=1, closeLeft, closeRight };
public:
	APoint sq[4];
	APoint center;
	float sideLen;
	
	// constructors and destructor
	XZSquare(void);
	XZSquare(const XZSquare& copy);
	~XZSquare(void);
	
	// set initial square values
	void Init(const APoint& vLocation, const AVector& vForward, const AVector& vRight, const float fSideLen);
	
	// set the square based on a center point in xyz, forward and right vector in xz, and square sideLen
	void Update(const APoint& vLocation, const AVector& vForward, const AVector& vRight);
	
	// getters
	APoint& operator [] (const int x);
	const APoint& operator [] (const int x) const;
	
	// send square object to ostream object
	friend std::ostream& operator << (std::ostream& os, const XZSquare& sq);
	
};	// end of class XZSquare


#endif	// XZ_SQUARE_H_




