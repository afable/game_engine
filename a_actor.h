//
//  a_actor.h
//  prog0-2_valentines
//
//  Created by afable on 12-04-28.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef A_ACTOR_H_
#define A_ACTOR_H_

#include <iostream>
#include <SDL/SDL.h>	// for handling events
#ifdef __linux__
	#include <GL/gl.h>
	#include <GL/glu.h>
#else
	#include <OpenGL/gl.h>
	#include <OpenGL/glu.h>
#endif
#include <vector>		// for collision checking (refer to SDL prog19-1)
#include <stdio.h>		// srand
#include <time.h>		// seed srand with time(NULL)
#include "a_matrix.h"
#include "a_preprocessor_defines.h"
#include "a_vector.h"
#include "xz_circle.h" // for circle collision detection
#include "xz_square.h" // for square collision detection


// world boundaries
const float WATER_LEVEL = -0.1f;
const float SEA_X_PLUS_BOUNDARY = 10.0f;
const float SEA_X_MINUS_BOUNDARY = -10.0f;
const float SEA_Z_PLUS_BOUNDARY = 10.0f;
const float SEA_Z_MINUS_BOUNDARY = -70.0f;
const float SEA_Y_BOUNDARY = -3.0f;
const float SKY_LEVEL = 40.5f;

// gravity and accelerations
const float GRAVITY = -0.02f;
const float ZERO_GRAVITY = 0.0f;
const float DOWN_ACCELERATION = -0.0005f;

// error ranges are used in calculating y value ranges in ProcessLogic() collisions
const float ERROR_RANGE_NORMAL = 0.04f;
const float ERROR_RANGE_SMALL = 0.02f;
const float ERROR_RANGE_SMALLEST = 0.01f;

// speed to move back from actor2block collisions
const float COLLISION_SPEED = 0.005f;

// determine how many frames until actor must be reset from being squished between two walls
const int SQUISH_AMOUNT_MAX = 33;

// actor jump values
const float JUMP_DECREMENT = -0.001f;
const int JUMP_DELAY_MAX = 33;
const float JUMP_SPEED = 0.09f;

/*----------------------------------------------------------
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|
 *|	AActor: base class for all actors within the scene including 'fake' eye.
 *| 
 *|	Class Hierarchy:
 *|
 *|		- AActor: base class for all actors within the scene including 'fake' eye.
 *|
 *|			- ABlock: a static block that makes up the ground terrain and island terrain.
 *|
 *|				- AElevator: moving blocks that make up moving platforms within game.
 *|
 *|					- ABoat: a special kind of elevator: moving blocks that are player controlled transport vessels.
 *|
 *|			- AEye: actor holds the position and direction of OpenGL's fake eye.
 *|
 *|			- AMonster: base class for a general monster.
 *|
 *|				- AWaterMonster: a monster that stays in the sea and jumps on approaching boats.
 *|
 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 *|_________________________________________________________
 */
// A GL orthonormal frame class
class AActor
{
private:
	
public:
	/*************************************************
	 * STATIC VARIABLES: statics generally used to avoid construction calls when performing
	 *	actor movements and collision detections
	 *************************************************/
	/* column ordered matrix:
	 *		right[0],	up[0],	fwd[0],	translation[0],
	 *		right[1],	up[1],	fwd[1],	translation[1],
	 *		right[2],	up[2],	fwd[2],	translation[2],
	 *		0.0f,		0.0f,	0.0f,	1.0f;
	 */
	static AMatrix m;
	static AVector vTemp;
	
	// box1 temp points in world coords
	static APoint left;		// leftmost vertex for box1
	static APoint right;	// rightmost vertex, etc...
	static APoint up;
	static APoint down;
	
	// collision test vectors for box1
	static AVector l2d;
	static AVector d2r;
	static AVector r2u;
	static AVector u2l;

	// box2 temp points in world coords
	static APoint left2;	// leftmost vertex for for box2, etc.
	static APoint right2;
	static APoint up2;
	static APoint down2;
	
	// collision test vectors for box2
	static AVector left2test;
	static AVector down2test;
	static AVector right2test;
	static AVector up2test;
	
	// circle2circle vector
	static AVector center;
	
	// circle2square vectors;
	static float dist[4];
	static AVector close[4];
	static AVector corner2corner;
	static AVector corner2center;
	static APoint closestPoint;
	static AVector closestVector;
	
	// for eye not moving but being pushed against walls
	static AVector pusher2pushee;
	
	
	/*************************************************
	 * MEMBER VARIABLES
	 *************************************************/
	unsigned int nId;	// should be assigned in program so actors in vector list do not collision detect themselves
	
	// actor position, size, and orientation
	APoint vLocation;	// where am I?
	AVector vForward;	// where am I going?
	AVector vUp;	// which way is up?
	AVector vRight;		// CrossProduct(vForward, vUp)
	float fHalfHeight;	// height of actor
	float fTopY;	// top of actor in y
	float fBotY;	// bottom of actor in y
	float fRotX;	// up and down rotation
	float fRotY;	// left and right rotation

	// collision data
	XZSquare boundingBox;	// for square collisions
	XZCircle boundingCircle;	// for circle collisions
	
	// velocities and speeds
	float fGravity;
	float fTransSpeed;
	float fForwardVelocity;
	float fBackwardVelocity;
	float fUpVelocity;
	float fDownVelocity;
	float fRightVelocity;
	float fLeftVelocity;
	float fRotSpeed;
	float fRotXVelocity;
	float fRotYVelocity;

	// collision data
	bool bCollision;	// general collision between actors
	bool bTopCollision;	// actor collide with block above?
	bool bBotCollision;	// actor collide with actor below?
	bool bInteractCollision;	// actor collide with another actor on same level?
	bool bEdge;	// actor on block edge?
	bool bCorner;	// actor on block corner?	
	int nSquished;	// countdown for actor being squashed between two blocks
	
	// jump data
	bool bCanJump;	// can actor jump?
	int nJumpDelay;	// delay between being able to jump
	
	// hit data
	bool bHit;	// was actor hit by another actor?
	
	// monster hearts
	bool bHeart;	// monster has been killed and turned into a heart
	bool bDelete;
	
	// reset data
	float fXLoc;
	float fYLoc;
	float fZLoc;
	float fXRot;
	float fYRot;
	
	
	/* CONSTRUCTORS & DESTRUCTOR */
	// default constructor
	AActor(void);
	//	AActor(const AActor& a);	use default copy constructor (deep copying unnecessary)
	//	AActor& operator = (AActor& a);	use default assignnment operator
	virtual ~AActor(void);	// since AActor is a base class, if we ever have a base object pointer/reference refering to a derived object type, we want to dynamic bind a call to the destructor of the object type (derived)! not to the pointer type (base). compiler uses static binding to pointer type (base) by default since it is known at compile time so we make this destructor virtual to solve that
	
	/* SETTERS */
	void LoadIdentity(void);	// simulate glLoadIdentity() -- reset unit vectors and bound angles (leave location alone) before rendering to screen
	void SetGravity(float g);
	
	/* GETTERS */
	float* GetMatrix(void);	// get transformation matrix from right,up,forward,location vectors
	
	/* TRANSLATE LOCAL COORD SYSTEM */
	void MoveRightX(float x);
	void MoveRightY(float x);
	void MoveRightZ(float x);
	void MoveUpX(float y);
	void MoveUpY(float y);
	void MoveUpZ(float y);
	void MoveForwardX(float z);
	void MoveForwardY(float z);
	void MoveForwardZ(float z);
	void MoveRightX(const AVector& right, float x);
	void MoveRightY(const AVector& right, float x);
	void MoveRightZ(const AVector& right, float x);
	void MoveUpX(const AVector& up, float y);
	void MoveUpY(const AVector& up, float y);
	void MoveUpZ(const AVector& up, float y);
	void MoveForwardX(const AVector& forward, float z);
	void MoveForwardY(const AVector& forward, float z);
	void MoveForwardZ(const AVector& forward, float z);
	void MoveDirectionX(const AVector& direction, float x);
	void MoveDirectionY(const AVector& direction, float y);
	void MoveDirectionZ(const AVector& direction, float z);
	
	/* ROTATE LOCAL COORD SYSTEM */
	void RotateX(float angle);
	void RotateY(float angle);
	void RotateZ(float angle);
	void Rotatef(float angle, float x, float y, float z);

	
	/*----------------------------------------------------------
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|
	 *| CIRCLE 2 CIRCLE COLLISION DETECTION: test for circle to circle collisions on xz plane: block
	 *|		outter bounding circles, eye, monsters
	 *|
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|_________________________________________________________
	 */
	bool Circle2CircleCollision(const XZCircle& c1, const XZCircle& c2);
	
	
	
	/*----------------------------------------------------------
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|
	 *| CIRCLE 2 SQUARE COLLISION DETECTION: test for circle to square collision on xz plane:
	 *|		eye to block, monster to block
	 *|
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|_________________________________________________________
	 */
	bool Circle2SquareCollision(const XZCircle& c, const XZSquare& box);
	
	
	
	/*----------------------------------------------------------
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|
	 *| CIRCLE 2 SQUARE COLLISION DETECTION: test for square to square collision on xz plane:
	 *|		block to block
	 *|
	 *|@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	 *|_________________________________________________________
	 */
	bool Square2SquareCollision(const XZSquare& box1, const XZSquare& box2);
	
	
	
	/* VIRTUAL FUNCTIONS */
	virtual void Reset(void);
	
	virtual void Init(void);
	virtual void Init(int monsterId, float xloc, float yloc, float zloc, float height, float radius, float transSpeed, float rotSpeed, float xrot, float yrot);	// function signature of a_monster.h
	virtual void Init(int blockId, float xloc, float yloc, float zloc, float height, float boxSideLen, float radius, float xrot, float yrot);	// function signature of a_block.h
	
	virtual void ProcessEvents(SDL_Event& event);
	virtual void ProcessLogic(void);
	virtual	void ProcessLogic(const std::vector<AActor*>& blocks, const std::vector<AActor*>& elevators);	// function signature of a_monster.h
	virtual void ProcessLogic(AActor* eye, const std::vector<AActor*>& blocks, const std::vector<AActor*>& elevators, std::vector<AActor*>& monsters);	// function signature of a_block.h
	
	virtual void Display(GLuint lDrawing);
	
	/* FRIEND FUNCTIONS */
	// send actor object to ostream object
	friend std::ostream& operator << (std::ostream& os, const AActor& a);
	
	
	
	
	


	
	
}; // end of class AActor


#endif // A_ACTOR_H_





