//
//  xz_circle.cpp
//  prog0-2_valentines
//
//  Created by afable on 12-05-03.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#include "xz_circle.h"

XZCircle::XZCircle(void)
{
	rad = 0.0f;
	center.Set(0.0f, 0.0f, 0.0f);
}

XZCircle::XZCircle(const XZCircle& c)
{
	rad = c.rad;
	center = c.center;
}

XZCircle::~XZCircle(void)
{
	// silent do-nothing destructor
}

// initialize circle with radius r
void XZCircle::Init(APoint vLocation, const float r)
{
	rad = r;
	Update(vLocation);
}

// set circle's radius
void XZCircle::Update(APoint vLocation)
{
	// bounding circles are flattened to the y=0 xz plane
	center.Set(vLocation[0], 0.0f, vLocation[2]);
}

std::ostream& operator << (std::ostream& os, const XZCircle& c)
{
	os << "\tcircle with radius: " << c.rad << std::endl;
	os << "\tcenter: " << c.center;
	
	return os;
}





